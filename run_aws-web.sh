#!/bin/sh 

USAGE="$0 [src-port dest-port]"


SRC_PORT=80
DEST_PORT=80

if [ "$1" != "" ]
then
	SRC_PORT=$1
fi
if [ "$2" != "" ]
then
	DEST_PORT=$2
fi
docker run -d -p ${SRC_PORT}:${DEST_PORT} registry.gitlab.com/swong-public/docker/aws-web:latest


#!/bin/sh

echo "Removing 'none' images ..."

docker rmi -f $(docker images -a | grep none | awk '{ print $3; }')

echo "removed 'none' images"

exited_containers="`docker ps -aq -f status=exited`"
if [ "${exited_containers}" != "" ]
then
	docker rm ${exited_containers}
fi

echo "removed 'exited' containers"

exit 0

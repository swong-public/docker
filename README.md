# Publicly accessible docker images

- aws-web
 - A simple web pannel into AWS
 - run it with docker run -d -p 80:80 registry.gitlab.com/swong-public/docker/aws-web:latest
- centos7 
 - latest centos7 image
- apache-lb
  - Apache load balancer with api to restart and update loadbalancer mambers
  - docker run -d -p 80:80 -p 84:84 registry.gitlab.com/swong-public/docker/apache-lb:latest
  - api is on port 84
  - test with http:[IP]/balancer-manager
  - api
    - restart with http:[IP]:84/api/lb_restart
    - update  with curl -X GET -D@new_members.json http:[IP]:84/api/lb_update
      E.g. new_members 
    - {"members": [
            {"host": "hostA", "port": "111", "min": "1", "max":"100", "loadfactor":"1", "timeout":"60", "route":"hostA" },
            {"host": "hostB", "port": "333", "min": "1", "max":"100", "loadfactor":"1", "timeout":"60", "route":"hostC" },
        ]
      }




